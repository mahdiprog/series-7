package sbu.cs;

import java.io.*;
import java.net.Socket;

public class ClientClass {
    private final static int PORT = 7170;
    private File file;
    private Socket client;
    private DataOutputStream dataOutputStream;
    private BufferedInputStream buffInFile;

    public ClientClass() {
        try {
            client = new Socket("127.0.0.1", PORT);
            System.out.println("connect to server!");
            dataOutputStream = new DataOutputStream(client.getOutputStream());
        }
        catch (Exception e){
            System.out.println("Somethings wrong in connecting to server!");
        }

    }

    public void sendFile(String fileName){
        file = new File(fileName);
        if (file.length() > Integer.MAX_VALUE) {
            System.out.println("Your file is too big!");
            return;
        }
        byte[] filedata = new byte[1024];
        try {
            dataOutputStream.writeUTF(fileName);
            buffInFile = new BufferedInputStream(new FileInputStream(file));
            BufferedOutputStream buffOutServer = new BufferedOutputStream(dataOutputStream);
            System.out.println("sending file...");
            int dataRead = 0;
            while((dataRead = buffInFile.read(filedata , 0 , filedata.length)) != -1 ){
                buffOutServer.write(filedata , 0 , dataRead);
            }
            buffOutServer.flush();
            System.out.println("file is sent!");
            System.out.println("file name " + fileName + "\nsize " + file.length() + " B");
            buffOutServer.close();
        }
        catch (Exception e){
            System.out.println("Somethings wrong in sending file");
        }
    }

    public void close(){
        try {
            System.out.println("Disconnecting...");
            buffInFile.close();
            dataOutputStream.close();
            client.close();
        }
        catch (Exception e){
            System.out.println("Somethings wrong in disconnecting");
        }
    }
}
