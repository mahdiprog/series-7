package sbu.cs;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerClass {
    private final static int PORT = 7170;
    private File file;
    private String fileName;
    private Socket socket;
    private ServerSocket server;
    private DataInputStream dataInputStream;
    private FileOutputStream fileOutFile;

    public ServerClass(){
        try {
            server = new ServerSocket(PORT);
            System.out.println("Server is Started!");
            System.out.println("connecting to client...");
            socket = server.accept();
            System.out.println("Client connected!");
            dataInputStream = new DataInputStream(socket.getInputStream());
        }
        catch (Exception e){
            System.out.println("Somethings wrong in connecting");
        }
    }

    public void request(String path){
        try {
            fileName = dataInputStream.readUTF();
            byte[] fileData = new byte[1024];
            file = new File(path + "/" + fileName);
            fileOutFile = new FileOutputStream(file);
            BufferedInputStream buffInClient = new BufferedInputStream(dataInputStream);
            System.out.println("receiving file \"" + path + "/" + fileName + "\" ...");
            int dataRead = 0;
            while((dataRead = buffInClient.read(fileData , 0 , fileData.length)) != -1 ){
                fileOutFile.write(fileData , 0 , dataRead);
            }
            System.out.println("Receive file!");
            System.out.println("file name " + fileName + "\nsize " + file.length() + " B" + "\npath ");
            buffInClient.close();
        }
        catch (Exception e){
            System.out.println("Somethings wrong in receiving file");
        }
    }

    public void close(){
        System.out.println("closing server...");
        try {
            socket.close();
            dataInputStream.close();
            fileOutFile.close();
            System.out.println("Server is close!");
        }
        catch (Exception e){
            System.out.println("somethings wrong in closing server!");
        }
    }
}